package com.example.cicd.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
        return "这是master分支";
    }

    @RequestMapping("/generic-webhook-trigger/invoke")
    public String test(@RequestParam("token") String token){
        System.out.println("接受的token为:"+token);
        return "token"+token;
    }
}
